import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { IdeasController } from './controllers/ideas.controller';
import { IdeasRepository } from './repositories/ideas.repository';
import { IdeaSchema } from './schemas/idea.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
         name: 'Idea', schema: IdeaSchema
      }
    ])
  ],
  controllers: [IdeasController],
  providers: [ IdeasRepository ]
})
export class IdeasModule {}