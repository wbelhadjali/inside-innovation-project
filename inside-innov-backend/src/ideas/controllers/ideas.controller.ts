import { Controller, Get } from '@nestjs/common';

@Controller('ideas')
export class IdeasController {
  @Get()
  findAll(): string {
    return 'This action returns all ideas';
  }
}