import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId } from 'mongoose';
import { Idea } from '../schemas/idea.schema';

@Injectable()
export class IdeasRepository {
  constructor(
    @InjectModel('Idea') private ideaModel: Model<Idea>) {}

    async getAllIdeas(): Promise<Idea[]> {
      const result =  await this.ideaModel.find();
      return result;
    }

    async addIdea(idea) {
      const newIdea = new this.ideaModel(idea);
      return newIdea.save();
    }
}