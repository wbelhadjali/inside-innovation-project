import { Module } from '@nestjs/common';
import { IdeasModule } from './ideas/ideas.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    IdeasModule,
    MongooseModule.forRoot('mongodb://localhost:27017/inside-innvoation-db')
  ],
  controllers: [],
  providers: [],
})
export class AppModule { }
